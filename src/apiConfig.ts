export const baseUrl = "https://api.themoviedb.org/3";
export const api_key = "19f84e11932abbc79e6d83f82d6d1045";
export const genres = [
    {
        path: `/trending/all/week?api_key=${api_key}&language=en-US`,
        key: 'trending'
    },
    {
        path: `/discover/tv?api_key=${api_key}&with_networks=213`,
        key: 'netflix-originals'
    },
    {
        path: `/movie/top_rated?api_key=${api_key}&language=en-US`,
        key: 'top-rated'
    },
    {
        path: `/discover/movie?api_key=${api_key}&with_genres=28`,
        key: 'action'
    },
   {
        path: `/discover/movie?api_key=${api_key}&with_genres=35`,
        key: 'comedy'
    },
    {
        path: `/discover/movie?api_key=${api_key}&with_genres=27`,
        key: 'horror'
    },
    {
        path: `/discover/movie?api_key=${api_key}&with_genres=10749`,
        key: 'romance'
    },
    {
        path: `/discover/movie?api_key=${api_key}&with_genres=99`,
        key: 'documentary'
    },
]
