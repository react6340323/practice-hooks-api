import { inject } from '@loopback/core';
import { DefaultCrudRepository } from '@loopback/repository';
import { MongodbDataSource } from '../datasources';
import { Movies, MoviesRelations } from '../models';
import { resolve } from 'path';
import { readdirSync, readFileSync, writeFileSync } from 'fs';
import { baseUrl, api_key, genres } from '../apiConfig'
import { moveCursor } from 'readline';

const rp = require('request-promise');

export class MoviesRepository extends DefaultCrudRepository<
  Movies,
  typeof Movies.prototype.id,
  MoviesRelations
> {
  constructor(@inject('datasources.mongodb') dataSource: MongodbDataSource) {
    super(Movies, dataSource);
  }

  private readonly options = {
    uri: '',
    qs: {
      api_key
    },
    json: true
  }

  public async migrate() {
    console.log('Migrating from TDMB api...');

    const getMovies = async (path: string) => {
      this.options.uri = `${baseUrl}${path}`;
      const opts = this.options;
      try {
        const data = await rp(opts);
        return data.results;
      } catch(err) {
        throw err;
      }
    }

    const getTrailer = async (id: string) => {
      this.options.uri = `${baseUrl}/movie/${id}/videos`;
      const opts = this.options;
      try {
        const response = await rp(opts);
        if (response.results && response.results.length > 0) {
          const trailers = response.results.filter((a: any) => a.type === "Trailer" && a.site === "YouTube");
          console.log('trailers', trailers);
          if (trailers.length > 0) {
            return trailers[0].key;
          }
        }
        return '';
      } catch(err) {
        return ''
      }
    }

    try {
      const moviePromises = new Array();
      genres.forEach((item) => {
        moviePromises.push(getMovies(item.path))
      });
      const moviesGroup = await Promise.all(moviePromises);
      moviesGroup.forEach((key, index) => {
        key.forEach((mov: { category: string; }) => {
          mov.category = genres[index].key;
        })
      });
      const movies = [].concat(...moviesGroup);

      movies.forEach(async (movie: Movies | any, index) => {
        movie.original_id = movie.id;
        if (movie.release_date) {
          movie.release_date = new Date(movie.release_date);
        } else if (movie.first_air_date) {
          movie.release_date = new Date(movie.first_air_date);
          delete movie.first_air_date;
        } else {
          movie.release_date = new Date();
        }

        if (movie.name) {
          movie.title = movie.name;
          movie.original_title = movie.name;
          delete movie.name;
        }

        if (movie.original_name) {
          movie.title = movie.original_name;
          movie.original_title = movie.original_name;
          delete movie.original_name;
        }

        if (!movie.backdrop_path) {
          movie.backdrop_path = movie.poster_path;
        }
        movie.trailerKey = await getTrailer(movie.id) || '';

        await this.create(movie);
        console.log(`(${index}) Migrating movie ${movie.title} | ${movie.category}`, movie);
      })
    } catch (err) {
      throw err;
    }
  }
}
